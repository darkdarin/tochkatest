<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Carbon\Carbon;

Route::get('/', function () {
    $currencies = config('currency.list');

    $list = array_keys($currencies);
    $rates = [];

    if (count($list)) {
        $rates = DB::select("
select a.* from rates as a inner join (
  select currency, max(date) as cur_date
  from rates
  group by currency
) as b
on b.currency = a.currency and a.date = cur_date
where a.currency in ('" . implode("','", $list) . "')
");
    }

    return view('welcome', [
        'rates' => $rates,
    ]);
});

Route::get('/getDynamic', function () {
    $from = Carbon::now()->subMonth(3);
    $to = Carbon::now();

    $data = \App\Rate::where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date')->get();

    $result = [];
    $sets = [];
    $labels = [];

    foreach ($data as $item) {
        if (!in_array($item->date->format('d.m.Y'), $labels)) $labels[] = $item->date->format('d.m.Y');
        $sets[$item['currency']][] = $item['value'];
    }

    $result['labels'] = $labels;
    foreach ($sets as $key => $set) {
        $color = 'rgb(' . mt_rand(0, 255) . ',' . mt_rand(0, 255) . ',' . mt_rand(0, 255) . ')';
        $result['datasets'][] = [
            'label' => $key,
            'strokeColor' => $color,
            'pointColor' => $color,
            'pointStrokeColor' => $color,
            'pointHighlightFill' => $color,
            'pointHighlightStroke' => $color,
            'data' => $set
        ];
    }

    return response()->json($result);
});

Route::get('/layout', function () {
    return view('layout');
});
