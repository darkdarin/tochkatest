<?php

namespace App\Console\Commands;

use App\Rate;
use Carbon\Carbon;

class GetCursOnDate extends CurrencyBase
{
    /**
     * Название и сигнатура команды
     * @var string
     */
    protected $signature = 'currency:date {date?}';

    /**
     * Описание команды
     * @var string
     */
    protected $description = 'Получение ежедневных курсов валют';

    /**
     * Сама команда
     */
    public function handle()
    {
        // если передана дата - берем ее, иначе - текущий день
        $date = $this->argument('date') ? strtotime($this->argument('date')) : time();

        // параметры для запросы
        $params = new \stdClass();
        $params->On_date = date("Y-m-d", $date) . 'T00:00:00';

        $response = $this->soap->GetCursOnDateXml($params);

        if ($response->GetCursOnDateXMLResult->any) {
            $xml = new \SimpleXMLElement($response->GetCursOnDateXMLResult->any);
            // смотрим, за какой день были получены курсы
            $day = new Carbon((string)$xml->attributes()->OnDate);

            $this->comment('Курсы валют на: ' . $day->format('d.m.Y'));

            // перебираем те валюты, которые нам нужны
            foreach (config('currency.list') as $currency => $code) {
                $fromDb = true;
                // если за указанный день курс указанной валюты уже есть в базе - то ничег не делаем
                if (!$rate = Rate::whereDate('date', $day)->where('currency', $currency)->first()) {
                    // иначе сохраним курс
                    $rate = new Rate([
                        'currency' => $currency,
                        'value' => $this->getRate($xml, $currency),
                        'date' => $day
                    ]);
                    $rate->save();
                    $fromDb = false;
                }

                $this->info($currency . ': ' . $rate->value . ($fromDb ? ' (БД)' : ' (Soap)'));
            }
        }
    }

    /**
     * Возвращает курс выбранной валюты из XML
     * @param \SimpleXMLElement $xml
     * @param string $currencyCode
     * @return float|int
     */
    protected function getRate($xml, $currencyCode)
    {
        $xPath = '/ValuteData/ValuteCursOnDate[VchCode="' . $currencyCode . '"]';
        $result = $xml->xpath($xPath);
        if (count($result) == 0) return 0;

        return (float)(string)$result[0]->Vcurs / (float)(string)$result[0]->Vnom;
    }
}
