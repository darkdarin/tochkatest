<?php

namespace App\Console\Commands;

use App\Rate;
use Carbon\Carbon;

class GetCursDynamic extends CurrencyBase
{
    /**
     * Название и сигнатура команды
     * @var string
     */
    protected $signature = 'currency:dynamic {from?} {to?}';

    /**
     * Описание команды
     * @var string
     */
    protected $description = 'Получение динамики ежедневных курсов валюты';

    /**
     * Сама команда
     */
    public function handle()
    {
        // получаем параметры, если не указаны - берем значения по умолчанию (за последние три года)
        $fromDate = $this->argument('from') ? strtotime($this->argument('from')) : Carbon::now()->subYear(3)->timestamp;
        $toDate = $this->argument('to') ? strtotime($this->argument('from')) : time();

        // параметры для запроса
        $params = new \stdClass();
        $params->FromDate = (date('Y-m-d', $fromDate)) . 'T00:00:00';
        $params->ToDate = (date('Y-m-d', $toDate)) . 'T00:00:00';

        // перебираем валюты, которые нам необходимо сохранить
        foreach (config('currency.list') as $currency => $code) {
            $this->info('Выгрузка динамики для валюты: ' . $currency);
            $params->ValutaCode = $code;

            $response = $this->soap->GetCursDynamicXml($params);

            if ($response->GetCursDynamicXMLResult->any) {
                $xml = new \SimpleXMLElement($response->GetCursDynamicXMLResult->any);

                $bar = $this->output->createProgressBar(count($xml->ValuteCursDynamic));

                // перебираем дни из ответа
                foreach ($xml->ValuteCursDynamic as $day) {
                    $date = new Carbon($day->CursDate);
                    $rateValue = (float)(string)$day->Vcurs / (float)(string)$day->Vnom;

                    // если курса за указанный день нет в базе
                    $rate = Rate::whereDate('date', $date)->where('currency', $currency)->first();
                    if (!$rate) {
                        // создаем
                        $rate = new Rate();
                        $rate->currency = $currency;
                        $rate->date = $date;
                    }

                    // указываем курс
                    $rate->value = $rateValue;
                    $rate->save();

                    $bar->advance();
                }

                $bar->finish();
                $this->comment('');
            }
        }

        $this->comment('Завершено!');

    }
}
