<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CurrencyBase extends Command
{
    /**
     * @var \SoapClient
     */
    protected $soap;

    public function __construct()
    {
        parent::__construct();

        $this->soap = new \SoapClient(config('currency.wsdl'));
    }
}