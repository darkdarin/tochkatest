/**
 * File Uploader
 */
upload = {
    init: function () {
        $(document).on('change', 'input[type="file"]', upload.showPreviewFile);
        $(document).on('click', '.file-upload .remove', upload.removeFile);

        $(document).on('dragover', '.file-upload', function () {
            $(this).addClass('hover');
            return false;
        });
        $(document).on('dragleave', '.file-upload', function () {
            $(this).removeClass('hover');
            return false;
        });
    },

    showPreviewFile: function (event) {
        var $container = $(this).parents('.file-upload');
        $container.removeClass('hover');

        var $input = $(this);

        var files = event.target.files;
        if (!files.length) {
            return false;
        }

        var file = files[0];
        var name = $container.data('name');

        $container.find('.file-upload-preview').find('span').remove();

        var fileReader = new FileReader();

        fileReader.onload = (function (theFile) {
            return function (e) {
                $container.addClass('preview');
                $container.find('.file-upload-preview').append('<span class="fa fa-file-text"></span>');
                $container.find('.file-upload-preview').append('<span class="name">' + file.name + '</span>');
            };
        })(fileReader);

        fileReader.readAsDataURL(file);

        $('input[name="' + name + '"]').remove();
        $input.attr('name', name).hide();
        $container.find('label').prepend('<input type="file"">');

    },
    removeFile: function (e) {
        e.preventDefault();
        var $container = $(this).parents('.file-upload');
        var name = $container.data('name');
        $('input[name="' + name + '"]').remove();

        $container.find('label').append('<input type="hidden" name="' + name + '" value="empty">');
        $container.removeClass('preview');

        $container.find('.file-upload-preview').find('span').remove();
    }
};

$(function () {
    upload.init();
});